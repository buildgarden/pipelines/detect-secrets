# detect-secrets pipeline

<!-- BADGIE TIME -->

[![pipeline status](https://img.shields.io/gitlab/pipeline-status/buildgarden/pipelines/detect-secrets?branch=main)](https://gitlab.com/buildgarden/pipelines/detect-secrets/-/commits/main)
[![latest release](https://img.shields.io/gitlab/v/release/buildgarden/pipelines/detect-secrets)](https://gitlab.com/buildgarden/pipelines/detect-secrets/-/releases)
[![pre-commit](https://img.shields.io/badge/pre--commit-enabled-brightgreen?logo=pre-commit)](https://github.com/pre-commit/pre-commit)
[![code style: prettier](https://img.shields.io/badge/code_style-prettier-ff69b4.svg)](https://github.com/prettier/prettier)

<!-- END BADGIE TIME -->

Secrets detection with [detect-secrets](https://github.com/Yelp/detect-secrets).

> Do not use this software unless you are an active collaborator on the
> associated research project.
>
> This project is an output of an ongoing, active research project. It is
> published without warranty, is subject to change at any time, and has not been
> certified, tested, assessed, or otherwise assured of safety by any person or
> organization. Use at your own risk.

## Usage

### GitLab CI

Add the following CI include to your `.gitlab-ci.yml` file:

```yaml
include:
  - project: buildgarden/pipelines/detect-secrets
    file:
      - detect-secrets.yml
```

### Pre-commit hook

Add the following hook to your `.pre-commit-config.yaml` file:

```yaml
repos:
  - repo: https://gitlab.com/buildgarden/pipelines/detect-secrets
    rev: ""
    hooks:
      - id: detect-secrets
```

Run `pre-commit autoupdate` to pull the latest version.

### Add options

Set the environment variable `DETECT_SECRETS_OPTS` with any additional
options for detect-secrests, such as `--exclude-files EXCLUDE_FILES_REGEX`.
